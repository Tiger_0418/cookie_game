﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PostNFCData : MonoBehaviour
{
    public InputField focusInput;  //卡片ID
    string url = "http://61.220.50.87:1188/API/GameScore";  // 傳送資料的API
    [SerializeField]
    //GameObject lastImg;
    //[SerializeField]
    //List<Sprite> startList;
    //[SerializeField]
    //Sprite scoreImg;
    //[SerializeField]
    //GameObject loadingImg;
    //[SerializeField]
    Text massText;
    int once = 0;
    public Text StationId_t;

    int count;
    int points;
    public GameObject mainsc;

    private void Start()
    {
        focusInput.Select();
        mainsc = GameObject.FindGameObjectWithTag("main");
    }
    private void Update()
    {
        if (focusInput.text.Length >= 8 && once == 0)
        {
            SendGameScore();
            once = 1;
        }

    }

    public void SendGameScore()
    {
        StartCoroutine(WaitForRequest());
    }

    IEnumerator WaitForRequest()
    {
        //Debug.Log("focusInput.text.Length  = " + focusInput.text.Length);
        focusInput.enabled = false;
        //loadingImg.SetActive(true);
        // 紀錄要傳送的資料
        var gs = new GameScore();
        gs.data = new GameScoreData();
        gs.data.CardId = focusInput.text;//"AD3D890E";
        gs.data.StationId = StationId_t.text;
        gs.data.GameId = "Game7";
        // json資料轉string
        string jsonStr = JsonUtility.ToJson(gs);

        // 傳送方式POST
        UnityWebRequest www = new UnityWebRequest(url, "POST");
        // 編碼
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonStr);
        // 帶資料
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        // 資料格式json
        www.SetRequestHeader("Content-Type", "application/json");

        // 送出
        yield return www.Send();

        //判斷www error
        if (www.isHttpError || www.isNetworkError)
        {
            Debug.Log("www error");

            massText.text = "網路傳輸失敗";
            //Invoke("ReStart", 2);
        }
        else
        {
            //傳送成功的回傳值 & 資料解碼
            string str = WWW.UnEscapeURL(www.downloadHandler.text);
            //string str = "{\"data\":\"{\"Games\":[{\"Count\":5,\"GameId\":\"\"}],\"Points\":1}\"}";
            //Debug.Log(str);
            //string countStr = str.Split(":"[0])[5];

            str = str.Substring(9, str.Length - (9 + 2));
            Debug.Log("new json=" + str);
            RespGameScoreGroup respData = FromJson(str);
            Debug.Log("respData=" + JsonUtility.ToJson(respData));

            count = respData.Games[0].Count;
            points = respData.Points;
            Debug.Log(count);
            Debug.Log(points);

            if (count >= 2)
            {
                mainsc.GetComponent<Main>().RFID.SetActive(false);
                mainsc.GetComponent<Main>().RFID_1.SetActive(false);
                mainsc.GetComponent<Main>().RFID_fail.SetActive(true);

                mainsc.GetComponent<Main>().restart.SetActive(true);
            }
            if (count == 1)
            {
                mainsc.GetComponent<Main>().RFID.SetActive(false);
                mainsc.GetComponent<Main>().RFID_1.SetActive(false);
                mainsc.GetComponent<Main>().RFID_sus.SetActive(true);

                mainsc.GetComponent<Main>().restart.SetActive(true);
            }
            if (points == 0)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_0;
            }
            if (points == 1)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_1;
            }
            if (points == 2)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_2;
            }
            if (points == 3)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_3;
            }
            if (points == 4)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_4;
            }
            if (points == 5)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_5;
            }
            if (points == 6)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_6;
            }
            if (points == 7)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_7;
            }
            if (points == 8)
            {
                mainsc.GetComponent<Main>().RFID_star.sprite = mainsc.GetComponent<Main>().star_8;
            }

            massText.text = "成功";
            /*for (int i = 0; i < count; i++)
            {
                //startList[i] = scoreImg;
            }*/
        }
        yield return null;
    }
    public void ReStart()
    {
        //GetComponent<GoScene>().GoSceneFun("MainScene");
    }

    private RespGameScoreGroup FromJson(string jsonStr)
    {
        return JsonUtility.FromJson<RespGameScoreGroup>(jsonStr);
    }
}

// json第一階data
[Serializable]
public class GameScore
{
    public GameScoreData data;
}

// data內三個參數
[Serializable]
public class GameScoreData
{
    public string CardId;
    public string StationId;
    public string GameId;
    public int Count;
}

[Serializable]
public class RespGameScore
{
    public int Count;
    public string GameId;
}

[Serializable]
public class RespGameScoreGroup
{
    public RespGameScore[] Games;
    public int Points;
}
