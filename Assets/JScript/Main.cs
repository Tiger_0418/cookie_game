﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public GameObject timeclock;
    public float time_count = 40;
    public float time_ten;
    public float time_one;
    public bool time_go;
    public bool click;
    public GameObject clickob;
    public GameObject target;
    public Vector3 offset;


    public GameObject frontpage;
    public GameObject allpage;
    int page_num = 2;

    public GameObject frontpage_1;

    public GameObject chose;
    public GameObject chose_1;
    public int chose_count;

    public GameObject foodpage;
    public GameObject foodpage_1;
    public GameObject allow_left;
    public GameObject allow_right;
    public GameObject foodright;
    public GameObject foodwrong;
    public float showtime;
    public GameObject hint_1;
    public GameObject hint_2;
    public GameObject hint_3;
    public GameObject hint_4;
    public GameObject hint_5;

    public GameObject foodpage_check;
    public GameObject foodpage_check_1;
    public GameObject fckpage_1;
    public GameObject fckpage_2;
    public GameObject fckpage_3;
    public GameObject fckpage_4;
    public GameObject fckpage_5;
    public GameObject fckpagecan_1;
    public GameObject fckpagecan_2;
    public GameObject fckpagecan_3;
    public GameObject fckpagecan_4;
    public GameObject fckpagecan_5;
    public int answer;

    public GameObject stir;
    public GameObject stir_1;
    public GameObject stir_stick;
    public int stir_count;
    public bool stir_left;
    public bool stir_right;
    public float stir_ten;
    public float stir_one;
    public RawImage ten_stir;
    public RawImage one_stir;

    public GameObject stamper;
    public GameObject stamper_1;
    public GameObject stamper_bg;
    public Sprite bg_1;
    public Sprite bg_2;
    public Sprite bg_3;
    public Sprite bg_4;
    public Sprite bg_5;
    public int stamper_count;
    public bool stampcheck;
    public GameObject stampfinish;
    public GameObject stamp_1;
    public GameObject stamp_2;
    public GameObject stamp_3;
    public GameObject stamp_4;
    public GameObject stamp_5;

    public GameObject clear;
    public GameObject clear_1;
    public GameObject stamptarget;
    public GameObject target_dog;
    public GameObject target_sun;
    public GameObject target_cat;
    public GameObject target_clow;
    public GameObject target_chick;


    public GameObject RFID;
    public GameObject RFID_1;
    public GameObject RFID_sus;
    public SpriteRenderer RFID_star;
    public GameObject RFID_fail;

    public GameObject restart;

    public GameObject fail;
    public GameObject fail_1;

    public GameObject icon_pepper;
    public GameObject icon_sugar;
    public GameObject icon_salt;
    public GameObject icon_milk;
    public GameObject icon_cream;
    public GameObject icon_mango;
    public GameObject icon_lemon;
    public GameObject icon_water;
    public GameObject icon_tea;
    public GameObject icon_flour;
    public GameObject icon_egg;
    public GameObject icon_Vanilla;
    public GameObject icon_nut;
    public GameObject icon_cocoa;
    public GameObject icon_Almond;
    public GameObject icon_Strawberry;
    public GameObject icon_cranberry;
    public GameObject icon_cheese;
    public GameObject icon_coconut;
    public GameObject icon_chocolate;

    public Sprite num_0;
    public Sprite num_1;
    public Sprite num_2;
    public Sprite num_3;
    public Sprite num_4;
    public Sprite num_5;
    public Sprite num_6;
    public Sprite num_7;
    public Sprite num_8;
    public Sprite num_9;

    public RawImage ten_num;
    public RawImage one_num;
    public AudioClip bingo;
    public AudioClip wrong;

    public GameObject setting_page;
    public InputField setting_1;
    public InputField setting_2;
    public InputField setting_3;
    public InputField setting_4;

    float time_2 = 45;
    float time_3 = 20;
    float time_4 = 55;

    public Sprite star_0;
    public Sprite star_1;
    public Sprite star_2;
    public Sprite star_3;
    public Sprite star_4;
    public Sprite star_5;
    public Sprite star_6;
    public Sprite star_7;
    public Sprite star_8;

    // Start is called before the first frame update
    void Start()
    {
        //time_count = PlayerPrefs.GetFloat("time_count");
        //time_2 = PlayerPrefs.GetFloat("time_2");
        //time_3 = PlayerPrefs.GetFloat("time_3");
        //time_4 = PlayerPrefs.GetFloat("time_4");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            setting_page.active = !setting_page.active;
        }
        if (foodright.activeSelf)
        {
            foodwrong.SetActive(false);
            showtime = showtime + 1f * Time.deltaTime;
        }
        if (foodwrong.activeSelf)
        {
            foodright.SetActive(false);
            showtime = showtime + 1f * Time.deltaTime;
        }
        if (showtime >= 2f)
        {
            foodwrong.SetActive(false);
            foodright.SetActive(false);
            showtime = 0;
        }

        time_ten = Mathf.Floor(time_count / 10);
        time_one = Mathf.Floor(time_count - time_ten*10);

        num_run(ten_num, time_ten);
        num_run(one_num, time_one);

        stir_ten = Mathf.Floor(stir_count / 10);
        stir_one = Mathf.Floor(stir_count - stir_ten * 10);

        num_run(ten_stir, stir_ten);
        num_run(one_stir, stir_one);

        if (click)
        {
            Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f);
            clickob.transform.position = Camera.main.ScreenToWorldPoint(newPosition) + offset;
        }


        if (time_go)
        {
            time_count = time_count - 1*Time.deltaTime;
        }

        if (foodpage.activeSelf && chose_count == 8)
        {
            timeclock.transform.localPosition = new Vector3(0,322,0);
            foodpage_check.SetActive(true);
            foodpage_check_1.SetActive(true);
            hint_fn();
            time_count = time_2;
            time_go = false;
            foodpage.SetActive(false);
            foodpage_1.SetActive(false);
        }
        if (time_count <= 0 && foodpage.activeSelf && chose_count < 8)
        {
            timeclock.SetActive(false);
            foodpage.SetActive(false);
            foodpage_1.SetActive(false);
            hint_1.SetActive(false);
            hint_2.SetActive(false);
            hint_3.SetActive(false);
            hint_4.SetActive(false);
            hint_5.SetActive(false);

            fail.SetActive(true);
            fail_1.SetActive(true);
            time_go = false;
        }

        if (foodpage_check.activeSelf && answer == 8)
        {
            timeclock.transform.localPosition = new Vector3(790, 322, 0);
            foodpage_check.SetActive(false);
            foodpage_check_1.SetActive(false);

            stir.SetActive(true);
            stir_1.SetActive(true);
            time_count = time_3;
            time_go = false;
        }
        if (time_count <= 0 && foodpage_check.activeSelf && answer < 8)
        {
            timeclock.SetActive(false);
            foodpage_check.SetActive(false);
            foodpage_check_1.SetActive(false);
            hint_1.SetActive(false);
            hint_2.SetActive(false);
            hint_3.SetActive(false);
            hint_4.SetActive(false);
            hint_5.SetActive(false);

            fail.SetActive(true);
            fail_1.SetActive(true);
            time_go = false;
        }
        //攪拌
        if (stir.activeSelf)
        {
            time_go = true;
            timeclock.transform.localPosition = new Vector3(830, 410, 0);
            if (stir_left && stir_right)
            {
                stir_count++;
                stir_left = false;
                stir_right = false;
            }
        }
        if (stir_count >= 15 && stir.activeSelf)
        {
            stamper.SetActive(true);
            stamper_1.SetActive(true);
            time_count = time_4;
            time_go = false;
            stir.SetActive(false);
            stir_1.SetActive(false);
        }
        if (time_count <= 0 && stir.activeSelf)
        {
            if (stir_count < 15)
            {
                stir.SetActive(false);
                stir_1.SetActive(false);

                fail.SetActive(true);
                fail_1.SetActive(true);
                time_go = false;
            }
        }
        //押花
        if (stamper.activeSelf)
        {
            if (stamper_count >= 10)
            {
                //stampfinish.SetActive(true);
                clear.SetActive(true);
                clear_1.SetActive(true);
                timeclock.SetActive(false);

                for (int x = 0; x < 15; x++)
                {
                    GameObject cookie_cn = Instantiate(stamptarget, clear_1.transform);
                    cookie_cn.transform.localPosition = new Vector3(Random.Range(-24.21f, -2.81f), Random.Range(0.36f, 7.22f), 0);
                    cookie_cn.transform.localEulerAngles = new Vector3(0, 0, Random.Range(0, 360));
                    if (stamper_bg.GetComponent<SpriteRenderer>().sprite == bg_1)
                    {
                        cookie_cn.GetComponent<SpriteRenderer>().sprite = cookie_cn.GetComponent<cookie>().style_1;
                    }
                    if (stamper_bg.GetComponent<SpriteRenderer>().sprite == bg_2)
                    {
                        cookie_cn.GetComponent<SpriteRenderer>().sprite = cookie_cn.GetComponent<cookie>().style_2;
                    }
                    if (stamper_bg.GetComponent<SpriteRenderer>().sprite == bg_3)
                    {
                        cookie_cn.GetComponent<SpriteRenderer>().sprite = cookie_cn.GetComponent<cookie>().style_3;
                    }
                    if (stamper_bg.GetComponent<SpriteRenderer>().sprite == bg_4)
                    {
                        cookie_cn.GetComponent<SpriteRenderer>().sprite = cookie_cn.GetComponent<cookie>().style_4;
                    }
                    if (stamper_bg.GetComponent<SpriteRenderer>().sprite == bg_5)
                    {
                        cookie_cn.GetComponent<SpriteRenderer>().sprite = cookie_cn.GetComponent<cookie>().style_5;
                    }
                }
                stamper.SetActive(false);
                stamper_1.SetActive(false);


                time_go = false;
            }
            else
            {
                time_go = true;
            }
        }
        if (time_count <= 0 && stamper.activeSelf)
        {
            if (stamper_count < 10)
            {
                stamper.SetActive(false);
                stamper_1.SetActive(false);
                timeclock.SetActive(false);

                fail.SetActive(true);
                fail_1.SetActive(true);
                time_go = false;
            }
        }

        if (page_num == 1)
        {
            allpage.transform.localPosition = new Vector3(80,0,0);
        }
        if (page_num == 2)
        {
            allpage.transform.localPosition = new Vector3(0, 0, 0);
        }
        if (page_num == 3)
        {
            allpage.transform.localPosition = new Vector3(-80, 0, 0);
        }
    }
    public void page_run(int xx)
    {
        if (xx == 1 && page_num > 1)
        {
            page_num--;
            allow_right.SetActive(true);
        }
        if (xx == 2 && page_num < 3)
        {
            page_num++;
            allow_left.SetActive(true);
        }
        if (xx == 1 && page_num == 1)
        {
            allow_left.SetActive(false);
        }
        if (xx == 2 && page_num == 3)
        {
            allow_right.SetActive(false);
        }
    }
    public void hint_fn()
    {
        if (target == hint_1)
        {
            hint_1.SetActive(true);
        }
        if (target == hint_2)
        {
            hint_2.SetActive(true);
        }
        if (target == hint_3)
        {
            hint_3.SetActive(true);
        }
        if (target == hint_4)
        {
            hint_4.SetActive(true);
        }
        if (target == hint_5)
        {
            hint_5.SetActive(true);
        }
    }
    public void close_fn()
    {
        time_go = true;
        hint_1.SetActive(false);
        hint_2.SetActive(false);
        hint_3.SetActive(false);
        hint_4.SetActive(false);
        hint_5.SetActive(false);
    }
    public void play_fn(int xx)
    {
        if (xx == 0)
        {
            frontpage.SetActive(false);
            frontpage_1.SetActive(false);

            chose.SetActive(true);
            chose_1.SetActive(true);
        }
        /*if (xx == 1)
        {
            stamper.SetActive(false);
            stamper_1.SetActive(false);

            clear.SetActive(true);
            clear_1.SetActive(true);
            timeclock.SetActive(false);
        }*/
        if (xx == 2)
        {
            clear.SetActive(false);
            clear_1.SetActive(false);

            RFID.SetActive(true);
            RFID_1.SetActive(true);
        }
    }
    public void choose_fn(int xx)
    {
        chose.SetActive(false);
        chose_1.SetActive(false);

        foodpage.SetActive(true);
        foodpage_1.SetActive(true);
        timeclock.SetActive(true);
        if (xx == 0)
        {
            hint_1.SetActive(true);
            target = hint_1;
            icon_coconut.tag = "right";
            icon_sugar.tag = "right";
            icon_flour.tag = "right";
            icon_Strawberry.tag = "right";
            icon_cranberry.tag = "right";
            icon_mango.tag = "right";
            icon_cream.tag = "right";
            icon_Vanilla.tag = "right";
            fckpage_1.SetActive(true);
            fckpagecan_1.SetActive(true);
            stamper_bg.GetComponent<SpriteRenderer>().sprite = bg_1;
        }
        if (xx == 1)
        {
            hint_2.SetActive(true);
            target = hint_2;
            icon_Almond.tag = "right";
            icon_sugar.tag = "right";
            icon_flour.tag = "right";
            icon_salt.tag = "right";
            icon_water.tag = "right";
            icon_egg.tag = "right";
            icon_pepper.tag = "right";
            icon_nut.tag = "right";
            fckpage_2.SetActive(true);
            fckpagecan_2.SetActive(true);
            stamper_bg.GetComponent<SpriteRenderer>().sprite = bg_2;
        }
        if (xx == 2)
        {
            hint_3.SetActive(true);
            target = hint_3;
            icon_milk.tag = "right";
            icon_sugar.tag = "right";
            icon_flour.tag = "right";
            icon_salt.tag = "right";
            icon_tea.tag = "right";
            icon_egg.tag = "right";
            icon_cream.tag = "right";
            icon_Vanilla.tag = "right";
            fckpage_3.SetActive(true);
            fckpagecan_3.SetActive(true);
            stamper_bg.GetComponent<SpriteRenderer>().sprite = bg_3;
        }
        if (xx == 3)
        {
            hint_4.SetActive(true);
            target = hint_4;
            icon_cream.tag = "right";
            icon_sugar.tag = "right";
            icon_flour.tag = "right";
            icon_cocoa.tag = "right";
            icon_chocolate.tag = "right";
            icon_egg.tag = "right";
            icon_Vanilla.tag = "right";
            icon_nut.tag = "right";
            fckpage_4.SetActive(true);
            fckpagecan_4.SetActive(true);
            stamper_bg.GetComponent<SpriteRenderer>().sprite = bg_4;
        }
        if (xx == 4)
        {
            hint_5.SetActive(true);
            target = hint_5;
            icon_sugar.tag = "right";
            icon_cheese.tag = "right";
            icon_flour.tag = "right";
            icon_lemon.tag = "right";
            icon_Almond.tag = "right";
            icon_nut.tag = "right";
            icon_cream.tag = "right";
            icon_Vanilla.tag = "right";
            fckpage_5.SetActive(true);
            fckpagecan_5.SetActive(true);
            stamper_bg.GetComponent<SpriteRenderer>().sprite = bg_5;
        }
    }
    public void setting_fn(int xx)
    {
        if (xx == 0)
        {
            Application.LoadLevel("Main");
        }
        if (xx == 1)
        {
            Application.Quit();
            PlayerPrefs.SetFloat("time_count", time_count);
            PlayerPrefs.SetFloat("time_2", time_2);
            PlayerPrefs.SetFloat("time_3", time_3);
            PlayerPrefs.SetFloat("time_4", time_4);
        }
        if (xx == 2)
        {
            setting_page.SetActive(false);
            time_count = float.Parse(setting_1.text);
            time_2 = float.Parse(setting_2.text);
            time_3 = float.Parse(setting_3.text);
            time_4 = float.Parse(setting_4.text);
        }
    }
    public void stir_fn(int xx)
    {
        if (xx == 1)
        {
            stir_stick.transform.localEulerAngles = new Vector3(0,0,50);
            stir_left = true;
        }
        if (xx == 2)
        {
            stir_stick.transform.localEulerAngles = new Vector3(0, 0, -50);
            stir_right = true;
        }
    }
    public void stamp_fn(int xx)
    {
        if (xx == 1 && stampcheck == false)
        {
            stamp_1.SetActive(true);
            stampcheck = true;
            stamptarget = target_dog;
        }
        if (xx == 2 && stampcheck == false)
        {
            stamp_2.SetActive(true);
            stampcheck = true;
            stamptarget = target_sun;
        }
        if (xx == 3 && stampcheck == false)
        {
            stamp_3.SetActive(true);
            stampcheck = true;
            stamptarget = target_chick;
        }
        if (xx == 4 && stampcheck == false)
        {
            stamp_4.SetActive(true);
            stampcheck = true;
            stamptarget = target_cat;
        }
        if (xx == 5 && stampcheck == false)
        {
            stamp_5.SetActive(true);
            stampcheck = true;
            stamptarget = target_clow;
        }
    }
    public void stampcount_fn()
    {
        stamper_count++;
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(180f);
        RFID.SetActive(false);
        RFID_1.SetActive(false);
    }
    public void num_run(RawImage yy,float xx)
    {
        if (xx == 0)
        {
            yy.texture = num_0.texture;
        }
        if (xx == 1)
        {
            yy.texture = num_1.texture;
        }
        if (xx == 2)
        {
            yy.texture = num_2.texture;
        }
        if (xx == 3)
        {
            yy.texture = num_3.texture;
        }
        if (xx == 4)
        {
            yy.texture = num_4.texture;
        }
        if (xx == 5)
        {
            yy.texture = num_5.texture;
        }
        if (xx == 6)
        {
            yy.texture = num_6.texture;
        }
        if (xx == 7)
        {
            yy.texture = num_7.texture;
        }
        if (xx == 8)
        {
            yy.texture = num_8.texture;
        }
        if (xx == 9)
        {
            yy.texture = num_9.texture;
        }
    }
}
